
import "package:over_react/over_react.dart";

@Factory()
UiFactory<ColorProps> Color;

@Props()
class ColorProps extends UiProps {
  // Props go here, declared as fields:
  String color;
  bool down;
  Function onMouseDown;
  Function onMouseUp;
  Function onMouseMouse;
}

@Component()
class ColorComponent extends UiComponent<ColorProps> {

  void _handleMouseMove(newColor) {
    if (props.down) {
      props.onMouseMove(props.color);
    }
  }

  void _handleMouseUp(e) => props.onMouseUp(e);

  void _handleMouseDown(e) => props.onMouseDown(e);

  @override
  render() {
    var colorStyles = {
      'backgroundColor': props.color,
      'height': 200,
      'width': 150
    };

    return(
      (Dom.div()
        ..className = 'color'
        ..style = colorStyles
        ..onMouseMove = _handleMouseMove
        ..onMouseDown = _handleMouseDown
        ..onMouseUp = _handleMouseUp
      )()
    );
  }
}
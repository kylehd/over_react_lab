
import "package:over_react/over_react.dart";
import 'Color.dart';

@Factory()
UiFactory<ColorDisplayProps> ColorDisplay;

@Props()
class ColorDisplayProps extends UiProps {
  // Props go here, declared as fields:
  String currentColor;
}

@Component()
class ColorDisplayComponent extends UiComponent<ColorDisplayProps> {

  @override
  render() {
    var displayStyles = {
      'display': 'flex',
      'justifyContent': 'center'
    };

    // Return the rendered component contents here.
    // The `props` variable is typed; no need for string keys!
    return(
      Dom.div()(
        Dom.h2()('Color Display'),
        (Dom.div()..style = displayStyles)(
          (Color()..color = props.currentColor)()
        )
      )
    );
  }
}
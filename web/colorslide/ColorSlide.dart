
import 'package:over_react/over_react.dart';
import 'Color.dart';

@Factory()
UiFactory<ColorSlideProps> ColorSlide;

@Props()
class ColorSlideProps extends UiProps {
  bool down;
  Function onMouseDown;
  Function onMouseUp;
  Function onMouseMouse;
}

@State()
class ColorSlideState extends UiState {
  List<String> colors;
}

@Component()
class ColorSlideComponent extends UiStatefulComponent<ColorSlideProps, ColorSlideState> {

  @override
  getInitialState() => (newState()
    ..colors = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']
  );

  void _handleMouseMove(newColor) => props.onMouseMove(newColor);

  void _handleMouseUp(e) => props.onMouseUp(e);

  void _handleMouseDown(e) => props.onMouseDown(e);

  render() {
    var slideStyles = {
      'display': 'flex',
      'justifyContent': 'center'
    };

    return (
      Dom.div()(
        Dom.h2()('Color Slide'),
        (Dom.div()
          ..style = slideStyles)(
          (state.colors.map((item) => (
            Color()
              ..color = item
              ..down = props.down
              ..onMouseMove = _handleMouseMove
              ..onMouseDown = _handleMouseDown
              ..onMouseUp = _handleMouseUp
          )()))
        ) 
      )
    );
  }
}
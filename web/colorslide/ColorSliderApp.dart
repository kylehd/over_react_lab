
import 'package:over_react/over_react.dart';

import 'ColorSlide.dart';
import 'ColorDisplay.dart';

@Factory()
UiFactory<ColorSliderAppProps> ColorSliderApp;

@Props()
class ColorSliderAppProps extends UiProps {}

@State()
class ColorSliderAppState extends UiState {
  String currentColor;
  bool down;
}

@Component()
class ColorSliderAppComponent extends UiStatefulComponent<ColorSliderAppProps, ColorSliderAppState> {

  Map getInitialState() => (newState()
    ..currentColor = 'black'
    ..down = false
  );

  _handleMouseMove(newColor) {
    if (state.down) {
      setState(newState()
        ..currentColor = newColor
        ..down = state.down
      ); 
    }
  }

  _handleMouseDown(e) {
    setState(newState()
      ..currentColor = state.currentColor
      ..down = true
    );
  }

  _handleMouseUp(e) {
    setState(newState()
      ..currentColor = state.currentColor
      ..down = false
    );
  }

  render() {
    return (
      Dom.div()(
        Dom.h2()('Color Slider App'),
        (ColorDisplay()
          ..currentColor = state.currentColor
        )(),
        (ColorSlide()
          ..down = state.down
          ..onMouseMove = _handleMouseMove
          ..onMouseDown = _handleMouseDown
          ..onMouseUp = _handleMouseUp
        )()
      )
    );
  }
}
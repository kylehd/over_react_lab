
import 'dart:html';
import 'package:react/react_dom.dart' as react_dom;
import 'package:react/react_client.dart' show setClientConfiguration;
import 'package:over_react/over_react.dart';
import 'colorslide/ColorSliderApp.dart';

main() {
  setClientConfiguration();
  react_dom.render(ColorSliderApp()(), querySelector('#react_mount_point'));
}